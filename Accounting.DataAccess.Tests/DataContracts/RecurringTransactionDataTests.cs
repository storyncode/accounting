using NUnit.Framework;
using Accounting.Common;
using Accounting.DataAccess.DataContracts;

namespace Accounting.DataAccess.Tests.DataContracts
{
  [TestFixture]
  public class RecurringTransactionDataTests : ContractTestBase
  {
    [Test]
    public void AccountId_SetsValue()
    {
      this.PropertyEqualsExpected(120, "AccountId");
    }

    [Test]
    public void Amount_SetsValue()
    {
      this.PropertyEqualsExpected(120.5m, "Amount");
    }

    [Test]
    public void Balance_SetsValue()
    {
      this.PropertyEqualsExpected(-10.42m, "Balance");
    }

    [Test]
    public void Frequency_SetsValue()
    {
      this.PropertyEqualsExpected(30, "Frequency");
    }

    [TestCase(FrequencyType.Day)]
    [TestCase(FrequencyType.Week)]
    [TestCase(FrequencyType.Month)]
    [TestCase(FrequencyType.Year)]
    public void FrequencyType_SetsValue(FrequencyType expected)
    {
      this.PropertyEqualsExpected(expected, "FrequencyType");
    }

    [Test]
    public void Id_SetsValue()
    {
      this.PropertyEqualsExpected(132480, "Id");
    }

    [Test]
    public void Reference_SetsValue()
    {
      this.PropertyEqualsExpected("DDAVIES", "Reference");
    }

    [TestCase(RecurringType.Bill)]
    [TestCase(RecurringType.Rent)]
    [TestCase(RecurringType.Savings)]
    [TestCase(RecurringType.Service)]
    [TestCase(RecurringType.Wage)]
    public void SubscriptionType_ReturnsExpected(RecurringType expected)
    {
      this.PropertyEqualsExpected(expected, "SubscriptionType");
    }

    [TestCase(TransactionType.Incoming)]
    [TestCase(TransactionType.Incoming)]
    public void TransactionType_ReturnsExpected(TransactionType expected)
    {
      this.PropertyEqualsExpected(expected, "TransactionType");
    }

    private void PropertyEqualsExpected<T>(T expected, string propertyName)
    {
      this.AssertPropertyEquals(expected, new RecurringTransactionData(), propertyName);
    }
  }
}