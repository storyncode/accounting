using NUnit.Framework;
using Accounting.DataAccess.DataContracts;

namespace Accounting.DataAccess.Tests.DataContracts
{
  [TestFixture]
  public class OwnerDataTests : ContractTestBase
  {
    [Test]
    public void Email_SetsValue()
    {
      this.PropertyEqualsExpected("test@email.com", "Email");
    }

    [Test]
    public void Id_SetsValue()
    {
      this.PropertyEqualsExpected(12093, "Id");
    }

    [Test]
    public void Name_SetsValue()
    {
      this.PropertyEqualsExpected("Owner name test", "Name");
    }

    private void PropertyEqualsExpected<T>(T expected, string propertyName)
    {
      this.AssertPropertyEquals(expected, new OwnerData(), propertyName);
    }
  }
}