using NUnit.Framework;
using Accounting.DataAccess.DataContracts;
using Accounting.Common;

namespace Accounting.DataAccess.Tests.DataContracts
{
  [TestFixture]
  public class TransactionDataTests : ContractTestBase
  {
    [Test]
    public void AccountId_SetsValue()
    {
      this.PropertyEqualsExpected(120, "AccountId");
    }

    [Test]
    public void Amount_SetsValue()
    {
      this.PropertyEqualsExpected(120.5m, "Amount");
    }

    [Test]
    public void Balance_SetsValue()
    {
      this.PropertyEqualsExpected(-10.42m, "Balance");
    }

    [Test]
    public void Id_SetsValue()
    {
      this.PropertyEqualsExpected(132480, "Id");
    }

    [Test]
    public void Reference_SetsValue()
    {
      this.PropertyEqualsExpected("DDAVIES", "Reference");
    }

    [TestCase(TransactionType.Incoming)]
    [TestCase(TransactionType.Incoming)]
    public void TransactionType_ReturnsExpected(TransactionType expected)
    {
      this.PropertyEqualsExpected(expected, "TransactionType");
    }

    private void PropertyEqualsExpected<T>(T expected, string propertyName)
    {
      this.AssertPropertyEquals(expected, new TransactionData(), propertyName);
    }
  }
}