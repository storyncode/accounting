using NUnit.Framework;
using System.Reflection;

namespace Accounting.DataAccess.Tests.DataContracts
{
  public class ContractTestBase
  {
    public void AssertPropertyEquals<T, U>(T expected, U actual, string propertyName)
    {
      PropertyInfo info = actual.GetType().GetProperty(propertyName);

      if (info != null && info.CanWrite)
      {
        info.SetValue(actual, expected, null);
      }

      if (info != null && !info.CanRead)
      {
        Assert.Fail();
        return;
      }

      Assert.AreEqual(expected, info.GetValue(actual));
    }
  }
}