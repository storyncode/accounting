using NUnit.Framework;
using Accounting.DataAccess.DataContracts;

namespace Accounting.DataAccess.Tests.DataContracts
{
  [TestFixture]
  public class AccountDataTests : ContractTestBase
  {
    [Test]
    public void Balance_SetsValue()
    {
      this.PropertyEqualsExpected(0.99m, "Balance");
    }

    [Test]
    public void Id_SetsValue()
    {
      this.PropertyEqualsExpected(99, "Id");
    }

    [Test]
    public void Name_SetsValue()
    {
      this.PropertyEqualsExpected("Account Name", "Name");
    }

    [Test]
    public void OwnerId_SetsValue()
    {
      this.PropertyEqualsExpected(1032490, "OwnerId");
    }

    private void PropertyEqualsExpected<T>(T expected, string propertyName)
    {
      this.AssertPropertyEquals(expected, new AccountData(), propertyName);
    }
  }
}