using Accounting.DataAccess.Daos;
using Accounting.DataAccess.DataContracts;
using LiteDB;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.DataAccess.Tests.Daos
{
  [TestFixture]
  public class AccountDaoTests : DaoTestBase
  {
    IEnumerable<AccountData> _data;

    [OneTimeSetUp]
    public void LocalOneTimeSetUp()
    {
      List<AccountData> instData = new List<AccountData>(); 
      using LiteDatabase db = new(_configuration.Object["DbFile"]);
      ILiteCollection<AccountData> col = db.GetCollection<AccountData>();

      for (int i = 1; i <= 10; i++)
      {
        AccountData data = new AccountData
        {
          Id = i,
          Name = $"{i}",
          OwnerId = i % 3,
          Balance = TestContext.CurrentContext.Random.NextDecimal() % 1000
        };

        instData.Add(data);

        col.Insert(data);
      }

      _data = instData;
    }

    [Test]
    public void GetAccount_ReturnsExpected()
    {
      AccountData actual;
      AccountData expected;
      int expectedId;
      AccountDao sut;

      expectedId = TestContext.CurrentContext.Random.Next(1, 10);

      expected = _data.First(x => x.Id == expectedId);

      sut = new AccountDao(_configuration.Object);

      actual = sut.GetAccount(expectedId);

      Assert.That(expected, Is.EqualTo(actual).Using(new AccountDataComparer()));
    }

    [Test]
    public void GetByOwner_ReturnsAllExpected()
    {
      IEnumerable<AccountData> actual;
      IEnumerable<AccountData> expected;
      int expectedOwnerId;
      AccountDao sut;

      expectedOwnerId = TestContext.CurrentContext.Random.Next(0, 3);

      expected = _data.Where(x => x.OwnerId == expectedOwnerId);

      sut = new AccountDao(_configuration.Object);

      actual = sut.GetByOwner(expectedOwnerId);

      Assert.That(expected, Is.EqualTo(actual).AsCollection
        .Using(new AccountDataComparer()));
    }

    [Test]
    public void Insert_SavesExpectedData()
    {
      AccountData actual;
      AccountData expected;
      AccountDao sut;

      expected = new AccountData()
      {
        Balance = 102308,
        Id = 10293,
        Name = "Account: 102308",
        OwnerId = 1212348
      };

      sut = new AccountDao(_configuration.Object);

      sut.Insert(expected);

      actual = sut.GetAccount(expected.Id);

      Assert.That(expected, Is.EqualTo(actual).Using(new AccountDataComparer()));
    }

    [Test]
    public void Insert_SetsIdIf0()
    {
      AccountData actual;
      AccountData expected;
      AccountDao sut;

      expected = new AccountData()
      {
        Balance = 102308.23m,
        Id = 0,
        Name = "Account: 302135",
        OwnerId = 1212348
      };

      sut = new AccountDao(_configuration.Object);

      sut.Insert(expected);

      actual = sut.GetAccount(expected.Id);

      Assert.AreNotEqual(0, actual.Id);
    }

    [Test]
    public void Update_SavesExpectedData()
    {
      AccountData actual;
      AccountData expected;
      int expectedId;
      AccountDao sut;

      expectedId = TestContext.CurrentContext.Random.Next(1, 10);

      expected = _data.First(d => d.Id == expectedId);

      sut = new AccountDao(_configuration.Object);

      actual = sut.GetAccount(expected.Id);

      expected.Balance += 607.91m;

      Assert.AreNotEqual(expected.Balance, actual.Balance);

      sut.Update(expected);

      actual = sut.GetAccount(expected.Id);

      Assert.That(expected, Is.EqualTo(actual).Using(new AccountDataComparer()));
    }
  }

  public class AccountDataComparer : IComparer<AccountData>
  {
    public int Compare(AccountData x, AccountData y)
    {
      int match = 0;

      if (x.Balance != y.Balance && match == 0)
        match = -1;

      if (x.Id != y.Id && match == 0)
        match = -1;

      if (x.Name != y.Name && match == 0)
        match = -1;

      if (x.OwnerId != y.OwnerId && match == 0)
        match = -1;

      return match;
    }
  }
}