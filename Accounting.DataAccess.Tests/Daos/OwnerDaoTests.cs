﻿using Accounting.DataAccess.Daos;
using Accounting.DataAccess.DataContracts;
using LiteDB;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Accounting.DataAccess.Tests.Daos
{
  [TestFixture]
  public class OwnerDaoTests : DaoTestBase
  {
    IEnumerable<OwnerData> _data;

    [OneTimeSetUp]
    public void LocalOneTimeSetUp()
    {
      List<OwnerData> instData = new List<OwnerData>();
      using LiteDatabase db = new(_configuration.Object["DbFile"]);
      ILiteCollection<OwnerData> col = db.GetCollection<OwnerData>();

      for (int i = 1; i <= 10; i++)
      {
        OwnerData data = new OwnerData()
        {
          Email = $"{i}@{i}.com",
          Id = i,
          Name = i.ToString()
        };

        instData.Add(data);

        col.Insert(data);
      }

      _data = instData;
    }

    [Test]
    public void GetAll_ReturnsAll()
    {
      IEnumerable<OwnerData> actual;
      IEnumerable<OwnerData> expected;
      OwnerDao sut;

      expected = _data;

      sut = new OwnerDao(_configuration.Object);

      actual = sut.GetAll();

      Assert.That(expected, Is.EqualTo(actual).AsCollection
        .Using(new OwnerDataComparer()));
    }

    [Test]
    public void GetOwner_ReturnsExpected()
    {
      OwnerData actual;
      OwnerData expected;
      int expectedId;
      OwnerDao sut;

      expectedId = TestContext.CurrentContext.Random.Next(1, 10);

      expected = _data.First(o => o.Id == expectedId);

      sut = new OwnerDao(_configuration.Object);

      actual = sut.GetOwner(expectedId);

      Assert.That(expected, Is.EqualTo(actual).Using(new OwnerDataComparer()));
    }

    [Test]
    public void Insert_SavesExpectedData()
    {
      OwnerData actual;
      OwnerData expected;
      OwnerDao sut;

      sut = new OwnerDao(_configuration.Object);

      expected = new OwnerData()
      {
        Email = "testing@id.com",
        Id = 140328,
        Name = "Test Owner: 140328"
      };

      sut.Insert(expected);

      actual = sut.GetOwner(expected.Id);

      Assert.AreNotEqual(0, actual.Id);
    }

    [Test]
    public void Insert_SetsIdIf0()
    {
      OwnerData actual;
      OwnerData expected;
      OwnerDao sut;

      sut = new OwnerDao(_configuration.Object);

      expected = new OwnerData()
      {
        Email = "testing@id.com",
        Id = 0,
        Name = "Test Owner: 1po32i849"
      };

      sut.Insert(expected);

      actual = sut.GetOwner(expected.Id);

      Assert.AreNotEqual(0, actual.Id);
    }

    [Test]
    public void Update_SavesExpectedData()
    {
      OwnerData actual;
      OwnerData expected;
      int expectedId;
      OwnerDao sut;

      expectedId = TestContext.CurrentContext.Random.Next(1, 10);

      expected = _data.First(d => d.Id == expectedId);
      expected.Email = "new@email.test";

      sut = new OwnerDao(_configuration.Object);

      sut.Update(expected);

      actual = sut.GetOwner(expected.Id);

      Assert.That(expected, Is.EqualTo(actual).Using(new OwnerDataComparer()));
    }
  }

  public class OwnerDataComparer : IComparer<OwnerData>
  {
    public int Compare(OwnerData x, OwnerData y)
    {
      int match = 0;

      if (x.Email != y.Email && match == 0)
        match = -1;

      if (x.Id != y.Id && match == 0)
        match = -1;

      if (x.Name != y.Name && match == 0)
        match = -1;

      return match;
    }
  }
}
