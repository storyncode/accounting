using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using System.IO;

namespace Accounting.DataAccess.Tests.Daos
{
  public class DaoTestBase
  {
    protected Mock<IConfiguration> _configuration;

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
      string path = Directory.GetCurrentDirectory() + "/test.db";

      _configuration = new Mock<IConfiguration>();

      _configuration.Setup(x => x["DbFile"]).Returns(path);

      if (File.Exists(path))
        File.Delete(path);
    }

    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
      string path = Directory.GetCurrentDirectory() + "/test.db";

      _configuration = null;

      if (File.Exists(path))
        File.Delete(path);
    }
  }
}