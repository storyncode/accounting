﻿using System;
namespace Accounting.Common
{
  public enum RecurringType
  {
    Bill,
    Rent,
    Savings,
    Service,
    Wage
  }
}
