﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Accounting.Common
{
    public enum FrequencyType
    {
      Day,
      Week,
      Month,
      Year
    }
}
