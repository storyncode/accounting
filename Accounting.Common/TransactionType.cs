﻿using System;
namespace Accounting.Common
{
  public enum TransactionType
  {
    Incoming,
    Outgoing
  }
}
