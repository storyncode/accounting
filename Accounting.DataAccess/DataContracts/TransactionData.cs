using Accounting.Common;

namespace Accounting.DataAccess.DataContracts
{
  public class TransactionData
  {
    public int AccountId {get;set;}
    public decimal Amount { get; set; }
    public decimal Balance { get; set; }
    public int Id { get; set; }
    public string Reference { get; set; }
    public TransactionType TransactionType { get; set; }
  }
}