namespace Accounting.DataAccess.DataContracts
{
  public class OwnerData
  {
    public string Email { get; set; }
    public int Id { get; set; }
    public string Name { get; set; }
  }
}