﻿namespace Accounting.DataAccess.DataContracts
{
  public class AccountData
  {
    public decimal Balance { get; set; }
    public int Id { get; set; }
    public string Name { get; set; }
    public int OwnerId { get; set; }
  }
}
