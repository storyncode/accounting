using Accounting.Common;

namespace Accounting.DataAccess.DataContracts
{
  public class RecurringTransactionData : TransactionData
  {
    public int Frequency { get; set; }
    public FrequencyType FrequencyType { get; set; }
    public RecurringType SubscriptionType { get; set; }
  }
}