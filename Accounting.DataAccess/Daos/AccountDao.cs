using Accounting.DataAccess.Daos.Interfaces;
using Accounting.DataAccess.DataContracts;
using LiteDB;

using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Accounting.DataAccess.Daos
{
  public class AccountDao : LiteDbDao, IAccountDao
  {
    public AccountDao(IConfiguration configuration) : base(configuration)
    {
      
    }

    public AccountData GetAccount(int id)
    {
      AccountData data = null;

      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<AccountData> collection;

        collection = db.GetCollection<AccountData>();

        data = collection.Query()
          .Where(ad => ad.Id == id)
          .First();
      }

      return data;
    }

    public IEnumerable<AccountData> GetByOwner(int ownerId)
    {
      IEnumerable<AccountData> data;

      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<AccountData> collection;

        collection = db.GetCollection<AccountData>();

        data = collection.Query()
          .Where(x => x.OwnerId == ownerId)
          .ToList();
      }

      return data;
    }

    public void Insert(AccountData account)
    {
      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<AccountData> collection;
        collection = db.GetCollection<AccountData>();

        if (account.Id == 0)
          account.Id = collection.Count();

        collection.Insert(account);
      }
    }

    public void Update(AccountData account)
    {
      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<AccountData> collection;
        collection = db.GetCollection<AccountData>();

        collection.Update(account);
      }
    }
  }
}