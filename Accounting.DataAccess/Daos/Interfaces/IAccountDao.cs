using Accounting.DataAccess.DataContracts;
using System.Collections.Generic;

namespace Accounting.DataAccess.Daos.Interfaces
{
  public interface IAccountDao
  {
    AccountData GetAccount(int id);
    IEnumerable<AccountData> GetByOwner(int ownerId);
    void Insert(AccountData account);
    void Update(AccountData account);
  }
}