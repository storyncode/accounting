using LiteDB;

namespace Accounting.DataAccess.Daos.Interfaces
{
  public interface IDao
  {
    LiteDatabase Open();
  }
}