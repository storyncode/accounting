using Accounting.DataAccess.DataContracts;

namespace Accounting.DataAccess.Daos.Interfaces
{
  public interface ITransactionDao
  {
    TransactionData GetByAccount(int accountId);
    TransactionData Insert(TransactionData transaction);
    TransactionData Update(TransactionData transaction);
  }
}