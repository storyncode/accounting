﻿using Accounting.DataAccess.DataContracts;
using System.Collections.Generic;

namespace Accounting.DataAccess.Daos.Interfaces
{
  public interface ISubscriptionDao
  {
    RecurringTransactionData GetSubscription(int id);
    IEnumerable<RecurringTransactionData> GetByOwner(int ownerId);
    void Insert(RecurringTransactionData subscription);
    void Update(RecurringTransactionData subscription);
  }
}
