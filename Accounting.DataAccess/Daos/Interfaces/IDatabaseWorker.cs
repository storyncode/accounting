﻿using LiteDB;
namespace Accounting.DataAccess.Daos.Interfaces
{
  public interface IDatabaseWorker<T>
  {
    ILiteCollection<T> GetCollection();
  }
}
