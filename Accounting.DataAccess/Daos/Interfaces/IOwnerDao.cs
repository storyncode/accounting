using Accounting.DataAccess.DataContracts;
using System.Collections.Generic;

namespace Accounting.DataAccess.Daos.Interfaces
{
  public interface IOwnerDao
  {
    IEnumerable<OwnerData> GetAll();
    OwnerData GetOwner(int id);
    void Insert(OwnerData owner);
    void Update(OwnerData owner);
  }
}