﻿using Accounting.DataAccess.Daos.Interfaces;
using Accounting.DataAccess.DataContracts;
using LiteDB;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Accounting.DataAccess.Daos
{
  public class OwnerDao : LiteDbDao, IOwnerDao
  {
    public OwnerDao(IConfiguration configuration) : base(configuration)
    {
    }
    
    public IEnumerable<OwnerData> GetAll()
    {
      IEnumerable<OwnerData> data;

      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<OwnerData> col = db.GetCollection<OwnerData>();

        data = col.Query()
          .ToList();
      }

      return data;
    }

    public OwnerData GetOwner(int id)
    {
      OwnerData data = null;

      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<OwnerData> collection;

        collection = db.GetCollection<OwnerData>();

        data = collection.Query()
          .Where(o => o.Id == id)
          .First();
      }

      return data;
    }

    public void Insert(OwnerData owner)
    {
      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<OwnerData> collection;

        collection = db.GetCollection<OwnerData>();

        if (owner.Id == 0)
          owner.Id = collection.Count() + 1;

        collection.Insert(owner);
      }
    }

    public void Update(OwnerData owner)
    {
      using (LiteDatabase db = this.Open())
      {
        ILiteCollection<OwnerData> collection;

        collection = db.GetCollection<OwnerData>();

        collection.Update(owner);
      }
    }
  }
}
