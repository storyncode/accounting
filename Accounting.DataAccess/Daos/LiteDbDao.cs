using Accounting.DataAccess.Daos.Interfaces;
using LiteDB;
using Microsoft.Extensions.Configuration;

namespace Accounting.DataAccess.Daos
{
  public abstract class LiteDbDao : IDao
  {
    private IConfiguration _configuration;

    public LiteDbDao(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    protected IConfiguration Configuration => _configuration;

    public LiteDatabase Open()
    {
      return new LiteDatabase(this.Configuration["DbFile"]);
    }
  }
}