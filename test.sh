# Remove comment if first run
# dotnet tool install dotnet-reportgenerator-globaltool --tool-path tools
dotnet test --logger "junit" --collect:"XPlat Code Coverage"
ls -ld $PWD/**/TestResults/*
./tools/reportgenerator "-reports:$PWD/**/TestResults/*/coverage.cobertura.xml" "-targetdir:Reports_Coverage" -reportTypes:TextSummary;
./tools/reportgenerator "-reports:$PWD/**/TestResults/*/coverage.cobertura.xml" "-targetdir:Reports_Coverage" -reportTypes:Html;
ls Reports_Coverage
cat ./Reports_Coverage/Summary.txt